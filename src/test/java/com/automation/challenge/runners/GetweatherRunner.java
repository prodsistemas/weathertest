package com.automation.challenge.runners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/weather/getweather.feature",
        format = {"html:target/cucumber-html-report", "json:target/cucumber-json-report.json" },
        glue = {"com.automation.challenge"}
)
public class GetweatherRunner {
}
