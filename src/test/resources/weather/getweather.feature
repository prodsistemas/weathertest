Feature: Get the current weather from OpenWeatherMap

  In order to get the weather
  As a user of the application
  I want to be able to get the weather using different ways

  Scenario Outline: Should get the weather by city name
    When I request the weather by city name '<city_name>'
    Then I should get the following weather: Description '<description>', Temperature '<temperature>', Temp Min'<temp_min>', Temp Max '<temp_max>'
    Examples:
      | city_name | description             | temperature | temp_min | temp_max |
      | London    | light intensity drizzle | 280.32      | 279.15   | 281.15   |

  Scenario Outline: Should get the weather by city ID
    When I request the weather by city ID '<city_id>'
    Then I should get the following weather: Description '<description>', Temperature '<temperature>', Temp Min'<temp_min>', Temp Max '<temp_max>'
    Examples:
      | city_id | description      | temperature | temp_min | temp_max |
      | 2905457 | scattered clouds | 300.15      | 300.15   | 300.15   |

  Scenario Outline: Should get the weather by geographic coordinates
    When I request the weather by geographic coordinates latitude: '<lat>', longitude: '<lon>'
    Then I should get the following weather: Description '<description>', Temperature '<temperature>', Temp Min'<temp_min>', Temp Max '<temp_max>'
    Examples:
      | lat | lon | description             | temperature | temp_min | temp_max |
      | 25  | 139 | clear sky               | 285.514     | 285.514  | 285.514  |