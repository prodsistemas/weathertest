package com.automation.challenge.definitions;

import com.automation.challenge.steps.WebServiceWeather;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;

public class GetweatherDefinition {

    @Steps
    WebServiceWeather webServiceWeather;
    Response response;

    @When("^I request the weather by city name '(.*)'$")
    public void iRequestTheWeatherByTheCityNameCity_name(String cityName) {
        response = webServiceWeather.getResponseByCityName(cityName);
    }

    @When("^I request the weather by city ID '(.*)'$")
    public void iRequestTheWeatherByTheCityID(String cityId) {
        response = webServiceWeather.getResponseByCityID(cityId);
    }

    @When("^I request the weather by geographic coordinates latitude: '(.*)', longitude: '(.*)'$")
    public void iRequestTheWeatherByTheGeographicCoordinates(String lat, String lon) {
        response = webServiceWeather.getResponseByGeographicCoordinates(lat, lon);
    }

    @Then("^I should get the following weather: Description '(.*)', Temperature '(\\d+\\.\\d+)', Temp Min'(\\d+\\.\\d+)', Temp Max '(\\d+\\.\\d+)'$")
    public void iShouldGetTheWeather(String description, float temperature, float tempMin, float tempMax) {
        webServiceWeather.verifyWeather(response, description, temperature, tempMin, tempMax);
    }

}
