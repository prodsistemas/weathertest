package com.automation.challenge.configuration;


import cucumber.api.java.Before;
import io.restassured.RestAssured;

public class RestAssuredConfiguration {

    @Before
    public void configure(){
        RestAssured.baseURI = "https://samples.openweathermap.org";
        RestAssured.basePath = "/data/2.5";
    }
}
