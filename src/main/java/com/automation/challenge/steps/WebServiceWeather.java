package com.automation.challenge.steps;

import com.automation.challenge.interfaces.EndPoint;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

public class WebServiceWeather extends ScenarioSteps {

    @Step
    public Response getResponseByCityName(String cityName) {
        return given().
                queryParam("q", cityName).
                queryParam("appid", EndPoint.GET_API_KEY).
                when().
                get(EndPoint.GET_ENDPOINT);
    }

    @Step
    public Response getResponseByCityID(String cityId) {
        return given().
                queryParam("id", cityId).
                queryParam("appid", EndPoint.GET_API_KEY).
                when().
                get(EndPoint.GET_ENDPOINT);
    }

    @Step
    public Response getResponseByGeographicCoordinates(String lat, String lon) {
        return given().
                queryParam("lat", lat).
                queryParam("lon", lon).
                queryParam("appid", EndPoint.GET_API_KEY).
                when().
                get(EndPoint.GET_ENDPOINT);
    }

    public void verifyWeather(Response response, String description, float temperature, float tempMin, float tempMax) {
        response.then().body(
                "weather.description", hasItem(description),
                "main.temp", is(temperature),
                "main.temp_min", is(tempMin),
                "main.temp_max", is(tempMax));
    }
}
