# Setting up WeatherTest project

## Pre-requirements
For run this project in your local machine, you need to have installed the following tools:
- An IDE installed such as IntelliJ.
- Java 1.8
- Gradle version 2.10 or latest.
Note: Remember to configure the environment variables of Java and gradle.

## Steps to run the tests
The following steps to run the tests are oriented to use the IDE Intellij IDEA.
1.- Clone the project from https://prodsistemas@bitbucket.org/prodsistemas/weathertest.git to a new folder called "WeatherTest".
2.- Open your favourite IDE (In this case, I have used IntelliJ).
3.- Import the project clicking on the build.gradle file into the WeatherTest project cloned.
4.- Once the import is finished, you are ready to run the test. To do that, you have some options:
    a.- From IDE.
    b.- From commands line.

### From IDE
- Click on Run -> Edit Configurations
- Add a new Configuration clicking on + button in the top left of the dialog and selecting Gradle.
- In the right of the dialog, select the tab Configuration and:
    a) Fill a name for this configuration.
    b) Select the Gradle project selecting the folder where is your project.
    c) Copy the commands "test aggregate" into the field Tasks.
    d) Apply and OK.
    e) Run the tests clicking on the green triangle.

### From commands line
- Open a command line terminal.
- Navigate to the project folder called "WeatherTest".
- Execute the sentence "gradle test aggregate".

## Checking the results
Once you have executed the tests either from the IDE or commands line, will appear a new folder called target in the root of the project.
For viewing the report, navigate through the folders: target -> site -> serenity and click on the index.html file.

For more information, you can contact me through:
- Phone: +573204126842
- Email: prodsistemas@gmail.com
- Skype: prodsistemas